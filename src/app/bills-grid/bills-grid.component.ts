import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import * as moment from 'moment';
import {PostsService} from '../posts.service';
import {Observable} from 'rxjs/Observable';

interface Post {
  price: number;
  fuel: number;
  date: Date;
  sum: number;
  isPaid: boolean;
}

@Component({
  selector: 'app-bills-grid',
  templateUrl: './bills-grid.component.html',
  styleUrls: ['./bills-grid.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class BillsGridComponent implements OnInit {
  public posts: Observable<Post[]>;
  public posts$: Observable<Post[]>;
  public dateFrom: Date;
  public dateTo: Date;

  constructor(public postsService: PostsService) {}

  ngOnInit() {
    this.getPosts();
    this.dateTo = moment().toDate();
    this.dateFrom = moment().subtract(3, 'month').toDate();
    this.selectPosts();

  }

onChangeDataPicker() {
    this.selectPosts();
}

  getPosts(): void {
    this.posts = this.postsService.getPosts();
  }

  selectPosts(): void {
    this.posts$ = this.postsService.selectPosts(this.dateFrom, this.dateTo);
  }

}
