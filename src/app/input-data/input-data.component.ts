import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import * as moment from 'moment';
import {PostsService} from '../posts.service';
import {GetCurrentPriceService} from '../get-current-price.service';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/first';


interface Post {
  price: number;
  fuel: number;
  date: Date;
  sum: number;
  isPaid: boolean;
}

@Component({
  selector: 'app-input-data',
  templateUrl: './input-data.component.html',
  styleUrls: ['./input-data.component.css'],
  encapsulation: ViewEncapsulation.None,

})
export class InputDataComponent implements OnInit {
  public posts: Observable<Post[]>;
  public newPost: Post;
  public overPaid: number;
  // public sum: number;

  constructor(public postsService: PostsService, public getCurrentPriceService: GetCurrentPriceService) {
  }

  ngOnInit() {
    this.newPost = {date: moment().toDate(), fuel: null, price: null, sum: 0, isPaid: false};
    this.getPosts();
    this.overPaid = 0;
  }

  getPosts(): void {
    this.posts = this.postsService.getPosts();

    this.getCurrentPriceService.getForToday()
      .first().subscribe(price => this.newPost.price = price);
  }

  addNewData(): void {
    if (this.overPaid > 0) {
      const sum = Number((this.newPost.fuel * this.newPost.price).toFixed(2));
      this.newPost.sum = (this.newPost.fuel - ((this.overPaid * this.newPost.fuel) / sum )) * this.newPost.price;
    } else {
      this.newPost.sum = Number((this.newPost.fuel * this.newPost.price).toFixed(2));
    }
    this.postsService.addNewData(this.newPost);
    this.newPost = {date: moment().toDate(), fuel: null, price: null, sum: 0, isPaid: false};
    this.getCurrentPriceService.getForToday()
      .first().subscribe(price => this.newPost.price = price);
    this.overPaid = 0;
  }
}





