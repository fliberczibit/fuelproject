import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {PostsService} from '../posts.service';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
interface Post {
  price: number;
  fuel: number;
  date: Date;
  sum: number;
  isPaid: boolean;
}
@Component({
  selector: 'app-total-summation',
  templateUrl: './total-summation.component.html',
  styleUrls: ['./total-summation.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class TotalSummationComponent implements OnInit {
  public sum$: Observable<string>;
  public posts: Observable<Post[]>;

  constructor(private postsService: PostsService) {
  }

  ngOnInit() {
    this.selectIsNotPaid();
  }

  selectIsNotPaid(): void {
    this.sum$ = this.postsService.selectIsNotPaid()
      .map(posts => this.calculateSum(posts)).map(sum => sum.toFixed(2));
  }

  calculateSum(posts: Post[]): number {
    return posts.reduce((prev, next) => prev + (isNaN(next.sum) ? 0 : next.sum), 0);
  }

  paySum() {
    return this.postsService.markAllUnpaidAsPaid();
  }
}
