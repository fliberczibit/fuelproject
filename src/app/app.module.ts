import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {
  MatButtonModule, MatCheckboxModule, MatDatepickerModule, MatFormFieldModule,
  MatNativeDateModule, MatInputModule
} from '@angular/material';
import {AppComponent} from './app.component';
import {InputDataComponent} from './input-data/input-data.component';
import { MomentModule } from 'angular2-moment';
import * as moment from 'moment';
import {NgModule} from '@angular/core';
import {AngularFireModule} from 'angularfire2';
import {AngularFirestoreModule} from 'angularfire2/firestore';
import {FormsModule} from '@angular/forms';
import {AngularFireDatabaseModule} from 'angularfire2/database-deprecated';
import {MatListModule} from '@angular/material';
import {MatTableModule} from '@angular/material';
import {MatCardModule} from '@angular/material';
import {HttpModule} from '@angular/http';
import { BillsGridComponent } from './bills-grid/bills-grid.component';
import {environment } from '../environments/environment';
import {PostsService} from './posts.service';
import { TotalSummationComponent } from './total-summation/total-summation.component';
import { GetCurrentPriceService } from './get-current-price.service';


@NgModule({
  declarations: [
    AppComponent,
    InputDataComponent,
    BillsGridComponent,
    TotalSummationComponent,

  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatNativeDateModule,
    MatInputModule,
    MomentModule,
    FormsModule,
    MatListModule,
    MatCardModule,
    MatTableModule,
    HttpModule,
    AngularFireModule,
    AngularFirestoreModule,
    AngularFireDatabaseModule,
    AngularFireModule.initializeApp(environment.firebase),
  ],
  providers: [PostsService, GetCurrentPriceService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
