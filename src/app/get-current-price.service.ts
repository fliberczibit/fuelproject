import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
@Injectable()
export class GetCurrentPriceService {

  constructor(private http: Http) {
  }

  getForToday(): Observable<number> {
    return this.http.get('https://cors-anywhere.herokuapp.com/http://www.auchan.pl/auchan-gdansk/o-sklepie')
      .map((data: Response) => {
        const html = data.text();
        const regexp = /class="boxType3-info">E 95: <span>(\d,\d\d) PLN/gm;
        const matches = regexp.exec(html);
        const price = matches[1].replace(',', '.');
        return parseFloat(price);
      });
  }
}
