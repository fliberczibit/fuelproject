import {Injectable} from '@angular/core';
import {AngularFirestore} from 'angularfire2/firestore';
import {DocumentChangeAction} from 'angularfire2/firestore/interfaces';
import {Observable} from 'rxjs/Observable';


interface Post {
  price: number;
  fuel: number;
  date: Date;
  sum: number;
  isPaid: boolean;
}

@Injectable()
export class PostsService {
  constructor(private db: AngularFirestore) {
  }

  getPosts(): Observable<Post[]> {
    return this.db.collection<Post>('posts').valueChanges();
  }

  selectPosts(dateFrom, dateTo): Observable<Post[]> {
    return this.db.collection<Post>('posts',
      ref => ref.where('date', '>', dateFrom).where('date', '<', dateTo))
      .valueChanges();
  }

  addNewData(post: Post) {
    return this.db.collection<Post>('posts').add(post);
  }

  selectIsNotPaid(): Observable<Post[]> {
    return this.db.collection<Post>('posts',
      ref => ref.where('isPaid', '==', false)).valueChanges();
  }


  markAllUnpaidAsPaid() {
    this.db.collection('posts', ref => ref.where('isPaid', '==', false)).snapshotChanges()
      .first()
      .subscribe((actions: DocumentChangeAction[]) => {
        actions.forEach(action => {
          action.payload.doc.ref.update({isPaid: true})
            .then(() => console.log('Success'))
            .catch((e) => console.log('Error', e));
        });
      });
  }
}


