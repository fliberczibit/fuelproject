// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyCk32oeUb98P8rVbMohrLV7MudioV2y1ZA',
    authDomain: 'testy-1f6e9.firebaseapp.com',
    databaseURL: 'https://testy-1f6e9.firebaseio.com',
    projectId: 'testy-1f6e9',
    storageBucket: 'testy-1f6e9.appspot.com',
    messagingSenderId: '825857877291'
  }
};

